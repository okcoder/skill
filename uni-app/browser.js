const browser = {
	/**
	 * 初始化
	 * @param options
	 * @returns {browser}
	 */
	init: function(options) {
		let opts = Object.assign({
			enabledPullRefresh: true ,// 是否开启默认下拉刷新,
			shrink:true,	// 悬浮框是否展开
			clickBackEvent: null
		}, options);
		this.initWebView(opts);
		this.handleEvent();
		return this;
	},
	initWebView: function(options) {
		let self = this;
		self.webView = plus.webview.create('', 'browser', {
			titleNView: {
				titleText: '',
				titleColor: '#FFFFFF',
				autoBackButton: false, // 在web-view中不需要这个按钮
				backgroundColor: '#000F1D',
				progress: {
					color: '#0000FF'
				},
				tags: [{
						id: 'bg',
						tag: 'img',
						src: '/static/nav_bg.png'
					},
					{
						id: 'img',
						tag: 'img',
						src: '/static/app_logo01.png',
						position: {
							top: 'auto',
							left: 'auto',
							width: '90px',
							height: '25px'
						}
					}
				],
				buttons: [{
					type: 'home',
					color: '#00B1F9',
					float: 'left',
					onclick: self.close.bind(self)
				}, {
					type: 'none',
					fontSrc: '/static/iconfont.ttf',
					text: '\ue651',
					fontSize: '20px',
					color: '#00B1F9',
					float: 'right',
					path: '/pages/ucenter/index/index'
				}]
			}
		});

		// 下拉刷新
		self.webView.setPullToRefresh({
			support: options.enabledPullRefresh
		}, function() {
			self.reload();
			self.webView.addEventListener('titleUpdate', function() {
				setTimeout(function() {
					self.webView.endPullToRefresh();
					self.webView.removeEventListener('titleUpdate', titleUpdate);
				}, 300);
			});

		});


		// 绘制悬浮框
		let screenWidth = plus.screen.resolutionWidth;
		console.warn('screenWidth', screenWidth);
		this.suspensionFrame = new plus.nativeObj.View("suspensionFrame", {
			bottom: '100px',
			left: '0px',
			height: '44px',
			width: (22 * 3 + 33) + 'px',
			backgroundColor: 'rgba(255,255,255,0.75)',
			position: 'dock',
			dock: 'left'
		});

		// 绘制收缩展开的样式
		const drawShrinkOpenButton = function(viewObj, type = 0) {
			const bitmap = new plus.nativeObj.Bitmap(type === 0 ? 'shrick' : 'open');
			bitmap.load(type === 0 ? '/static/browser/DirectLeft.png' : '/static/browser/DirectRight.png');
			viewObj.drawRect(bitmap, {
				width: '22px',
				height: '22px',
				left: '77px',
				top: '11px'
			});
			viewObj.show();
		}


		this.suspensionFrame.draw([{
			tag: 'img',
			id: 'hall',
			text: '大厅',
			src: '/static/browser/Bank.png',
			position: {
				width: '22px',
				height: '22px',
				left: '11px',
				top: '11px'
			}
		}, {
			tag: 'img',
			id: 'ucenter',
			text: '我的',
			src: '/static/browser/User.png',
			position: {
				width: '22px',
				height: '22px',
				left: '44px',
				top: '11px'
			}
		}, {
			tag: 'img',
			id: 'shrink',
			text: '收缩',
			src: '/static/browser/DirectLeft.png',
			position: {
				width: '22px',
				height: '22px',
				left: '77px',
				top: '11px'
			}
		}]);

		this.suspensionFrame.addEventListener('click', (e) => {
			if (0 <= e.clientX && e.clientX < 44) {
				// hall
			}
			if (44 <= e.clientX && e.clientX < 88) {
				// user
			}
			if (88 <= e.clientX) {
				// 收缩展开
				if(options.shrink){
					drawShrinkOpenButton(this.suspensionFrame,1)
					options.shrink = false;
				}else{
					drawShrinkOpenButton(this.suspensionFrame,0)
					options.shrink = true;
				}
				
			}
		});




		this.suspensionFrame.show();


		// 监听页面变化
		self.webView.addEventListener('loaded', function(e) {


			console.warn('............', e);

			let tn = self.webView.getStyle().titleNView;
			self.webView.canBack(function(event) {
				let canBack = event.canBack;
				if (!canBack) {
					if (tn.buttons[0].type == 'back') {
						// 隐藏返回按钮
						tn.buttons.splice(0, 1);
					}
				} else {
					if (tn.buttons[0].type != 'back') {
						// 添加返回按钮
						tn.buttons.unshift({
							type: 'back',
							color: '#00B1F9',
							float: 'left',
							onclick: self.back.bind(self)
						});
					}
				}
				self.webView.setStyle({
					titleNView: tn
				});
			});
		});


		self.webView.addEventListener('hashchange', function() {
			console.warn('-----------hashchange');
		});
		self.webView.addEventListener('popstate', function() {
			console.warn('-----------popstate');
		});
		self.webView.addEventListener('pushState', function() {
			console.warn('-----------pushState');
		});
		self.webView.addEventListener('replaceState', function() {
			console.warn('-----------replaceState');
		});

	},
	handleEvent: function() {
		let self = this;
		plus.key.addEventListener('backbutton', function() {
			var topWebview = plus.webview.getTopWebview();
			// 不等于浏览器窗口
			if (topWebview.id !== 'browser') {
				// 这里除了浏览器窗口就是首页了，直接退出了；
				plus.runtime.quit();
			} else {
				self.back();
			}
		});
	},
	show: function(url) {
		this.webView.loadURL(url);
		this.webView.show('slide-in-right');
	},
	reload: function() {
		this.webView.reload(true);
	},
	back: function() {
		let self = this;
		self.webView.canBack(function(event) {
			event.canBack ? self.webView.back() : self.close();
		});
	},
	//前进
	forward: function() {
		var self = this;
		self.webView.canForward(function(event) {
			event.canForward ? self.webView.forward() : plus.nativeUI.toast('没有可前进的地址');
		});
	},
	// 关闭
	close: function() {
		this.webView.close('browser', 'slide-out-right');
		this.webView.clear();
	}
};
export default browser;
